<?php

namespace Tests\Feature;

use App\Http\Controllers\LeagueController;
use App\Http\Controllers\MatchController;
use App\Models\Match;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\CreatesApplication;
use Tests\MigrateFreshSeedOnce;
use Tests\TestCase;

class MatchSimulationTest extends TestCase
{
    use CreatesApplication,
        MigrateFreshSeedOnce;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_match_simulation()
    {
        $matchController = new MatchController();
        $league_id = 1;
        LeagueController::createLeagueFixture(1);
        $leagueMatches = Match::whereHas('home_team', function ($query) use ($league_id) {
            $query->where('league_id', $league_id);
        })->get();
        $allTeamIds = $matchController->getAllTeamIds($leagueMatches);
        $resultRatioSet = $matchController->createResultingRatioSet($allTeamIds);
        $simulationResult = $matchController->simulateOnce($leagueMatches,$allTeamIds);
        foreach ($simulationResult as $key){
            $resultRatioSet[$key] += (1 / sizeof($simulationResult));
        }
        $this->assertGreaterThan(0, max($resultRatioSet));
    }
}
