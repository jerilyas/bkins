<?php

namespace Database\Seeders;

use App\Models\League;
use App\Models\Team;
use App\Models\Teams;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Premiere - 4
        $leagueFiles = Storage::allFiles('public/states');
        foreach ($leagueFiles as $leagueFile){
            $currentLeagueFile = Storage::get($leagueFile);
            $leagueData = collect(json_decode($currentLeagueFile));
            $this->insertLeagueData($leagueData,4);
            $this->insertLeagueData($leagueData);
        }
    }

    public function insertLeagueData($leagueData,$limit=false){
        $newLeague = new League();
        $newLeague->name = ($limit ? '[Only '.$limit.'] ' : '').$leagueData['name'];
        $newLeague->save();
        $leagueData->clubs = collect($leagueData['clubs']);
        if($limit){
            $leagueData->clubs = $leagueData->clubs->random($limit);
        }
        $all_teams_to_insert = [];
        $timeStamp = Carbon::now();
        foreach ($leagueData->clubs as $club){
            $all_teams_to_insert[] = ['name' => $club->name,'country' => $club->country,'code' => ($club->code ?? Str::upper(Str::limit($club->name))),'league_id' => $newLeague->id,'attack_strength' =>  mt_rand(1,100),'defence_strength' =>  mt_rand(1,100), 'created_at' => $timeStamp, 'updated_at' => $timeStamp];
        }
        DB::table('teams')->insert($all_teams_to_insert);
    }
}
