@extends('layout.base.master')

@section('content')
    <div
        class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        <div class="progress position-fixed d-none" style="top: 0">
            <div class="indeterminate"></div>
        </div>
        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                Please Select League
            </div>
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                <ul class="collection m-0" style="overflow-y: auto;max-height: 500px;background: black;margin:0">
                    @foreach($leagues as $league)
                        <li class="collection-item">
                            <div>{{ $league->name }}<a href="/league-fixture/{{ $league->id }}"
                                                       class="secondary-content">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-arrow-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                              d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                                    </svg>
                                </a></div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
            </div>
        </div>
@endsection
