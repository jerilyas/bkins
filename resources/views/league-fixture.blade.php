@extends('layout.base.master')

@section('content')
    <div style="position: absolute;top: 3px;right: 2px">
            <a href="/"  class="waves-effect waves-light btn blue">Change League <svg style="display: inline" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-90deg-left" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M1.146 4.854a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H12.5A2.5 2.5 0 0 1 15 6.5v8a.5.5 0 0 1-1 0v-8A1.5 1.5 0 0 0 12.5 5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4z"/>
                </svg></a>
    </div>
    <div
        class="relative flex items-top  min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        <div class="progress position-fixed d-none" style="top: 0">
            <div class="indeterminate"></div>
        </div>
        <div class="container-fluid">
            <div class="row align-content-center text-center">
                <div class="col-md-4 offset-sm-0 col-sm-12 p-2">
                    <a href="javascript:playAllMatches({{ $league->id }})"  class="waves-effect waves-light btn">Play All Weeks <svg style="display: inline" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-double-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z"/>
                            <path fill-rule="evenodd" d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z"/>
                        </svg></a>
                </div>
                <div class="col-md-4 col-sm-12 p-2">
                    <a href="javascript:playWeekMatch({{ $league->id }},{{ $current_week + 1 }})" class="waves-effect waves-light btn">Play Next Week<svg style="display: inline" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                        </svg></a>
                </div>
                <div class="col-md-4 col-sm-12 p-2">
                    <a href="javascript:resetData({{ $league->id }})"  class="waves-effect waves-red red btn">Reset Data <svg style="display: inline" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bootstrap-reboot" viewBox="0 0 16 16">
                            <path d="M1.161 8a6.84 6.84 0 1 0 6.842-6.84.58.58 0 1 1 0-1.16 8 8 0 1 1-6.556 3.412l-.663-.577a.58.58 0 0 1 .227-.997l2.52-.69a.58.58 0 0 1 .728.633l-.332 2.592a.58.58 0 0 1-.956.364l-.643-.56A6.812 6.812 0 0 0 1.16 8z"/>
                            <path d="M6.641 11.671V8.843h1.57l1.498 2.828h1.314L9.377 8.665c.897-.3 1.427-1.106 1.427-2.1 0-1.37-.943-2.246-2.456-2.246H5.5v7.352h1.141zm0-3.75V5.277h1.57c.881 0 1.416.499 1.416 1.32 0 .84-.504 1.324-1.386 1.324h-1.6z"/>
                        </svg></a>
                </div>
            </div>
            <div class="row replay" data-current_week="{{ $current_week }}">
                <div class="col-md-4">
                    @include('league-fixture-components.team-fixture')
                </div>
                <div class="col-md-4">
                    @include('league-fixture-components.match-results')
                </div>
                <div class="col-md-4">
                    @include('league-fixture-components.prediction')
                </div>
            </div>
        </div>
        <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
        </div>
@endsection

@section('page-scripts')
    <script>
        async function playWeekMatch(league_id){
            $('.progress').removeClass('d-none');
            return await $.ajax({
                type: "GET",
                url: '/play-week-matches/'+league_id,
                success: res => {
                    reloadPageContent(league_id);
                },
                error: err => {},
            });
        }
        async function resetData(league_id){
            $('.progress').removeClass('d-none');
            return await $.ajax({
                type: "GET",
                url: '/reset-matches/'+league_id,
                success: res => {
                    reloadPageContent(league_id);
                },
                error: err => {},
            });
        }

        async function reloadPageContent(league_id) {
            $.ajax({
                type: "GET",
                url: '/league-fixture/'+league_id,
                success: res => {
                    var update = $(res).find('.replay')
                    $('.replay').html(update.html());
                    $('.progress').addClass('d-none');
                },
                error: err => {},
            });
        }

        async function playAllMatches(league_id){
            $('.progress').removeClass('d-none');
            return await $.ajax({
                type: "GET",
                url: '/play-all-weeks/'+league_id,
                success: res => {
                    reloadPageContent(league_id);
                },
                error: err => {},
            });
        }
    </script>
@endsection
