
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="flex justify-center">
                Match Results
            </div>
        </div>
    </div>
    <div class="card-body">
        <h6>{{$active_week}} Week Match Results</h6>
        <table class="table table-bordered bg-white">
            <tbody>
                @foreach($matches as $match)
                <tr>
                    <td>{{ $match->home_team->name }}</td>
                    <td>{!!  $match->played ? $match->home_score : '-'  !!}</td>
                    <td>{{ $match->played ? $match->away_score : '-'}}</td>
                    <td>{{ $match->away_team->name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
