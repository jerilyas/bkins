
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <div class="flex justify-center">
                {{$active_week}} Week Predictions Of Championship
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered bg-white">
            <tbody>
            @foreach($league->teams as $team)
                <tr>
                    <td>{{ $team->name }}</td>
                    <td>{{ number_format($team->win_ratio,2,'.',',') }}%</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
