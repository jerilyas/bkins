
<div class="card blue">
    <div class="card-header">
        <div class="card-title">
            <div class="flex justify-center">
                {{ $league->name }}
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered" style="background-color: rgba(255,255,255,0.4)">
            <thead>
            <tr>
                <th>Teams</th>
                <th>PTS</th>
                <th>P</th>
                <th>W</th>
                <th>D</th>
                <th>L</th>
                <th>GD</th>
            </tr>
            </thead>
            <tbody>
            @foreach($league->teams->sortByDesc('pts') as $team)
                <tr>
                    <td>{{ $team->name }}</td>
                    <td>{{ $team->pts }}</td>
                    <td>{{ $team->played }}</td>
                    <td>{{ $team->win }}</td>
                    <td>{{ $team->d }}</td>
                    <td>{{ $team->l }}</td>
                    <td>{{ $team->goal_difference }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
