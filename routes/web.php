<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LeagueController;
use App\Http\Controllers\MatchController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LeagueController::class, 'newLeague']);
Route::get('/league-fixture/new/{league_id}', [LeagueController::class, 'createLeagueFixture']);
Route::get('/league-fixture/{league_id}', [LeagueController::class, 'leagueFixture']);
Route::get('/play-week-matches/{league_id}', [MatchController::class, 'playWeekMatches'])->name('play-week-matches');
Route::get('/play-all-weeks/{league_id}', [MatchController::class, 'playAllWeekMatches'])->name('play-all-week-matches');
Route::get('/reset-matches/{league_id}', [MatchController::class, 'resetLeagueMatches'])->name('reset-league-matches');

Route::group(["prefix" => "user" , "namespace" => "User"], function (){
});
