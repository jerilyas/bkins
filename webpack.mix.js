const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);

mix.sass('resources/sass/materialize.scss', 'public/css/all.min.css');

mix.css('resources/css/bootstrap.min.css', 'public/css/bootstrap.min.css');

mix.scripts([
    'resources/js/bootstrap.min.js',
    'resources/js/bin/materialize.min.js'
], 'public/js/all.min.js');
