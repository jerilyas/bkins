<?php

namespace App\Http\Controllers;

use App\Models\League;
use App\Models\Match;
use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MatchController extends Controller
{

    public static function getLeaguePlayedMatches($league_id){
        return League::where('id', $league_id)->with('teams', function ($query_1) {
            $query_1->with('home_matches', function ($q2) {
                $q2->where('played', 1);
            })->with('away_matches', function ($q3) {
                $q3->where('played', 1);
            });
        })->first();
    }

    public static function getWeekMatches($league_id, $week)
    {
        return Match::whereHas('home_team', function ($query) use ($league_id) {
            $query->where('league_id', $league_id);
        })->where('week', $week)->with('home_team')->with('away_team')->get();
    }

    public function playWeekMatches($league_id, $week = null)
    {
        if ($week == null) {
            $totalWeek = $this->getTotalWeek($league_id);
            $week = LeagueController::getLeagueWeek($league_id) + 1;
            if ($week > $totalWeek) {
                return;
            }
        }
        $weekMatches = MatchController::getWeekMatches($league_id, $week)->where('played', 0);
        foreach ($weekMatches as $match) {
            $this->playMatch($match);
            $match->save();
        }
        LeagueController::setLeagueWeek($league_id, $week);
    }

    public function playMatch($match)
    {
        $match->home_score = 0;
        $match->away_score = 0;
        // Home Team Goal Strength
        $home_team_diff = $match->home_team->attack_strength - $match->away_team->defence_strength;
        $home_team_diff = $home_team_diff + 5; // Small favor for home team
        // Away Team Goal Strength
        $away_team_diff = $match->away_team->attack_strength - $match->home_team->defence_strength;
        $away_team_diff = $away_team_diff - 5;
        // 16 to 24 different attack for per match
        for ($i = 0; $i < mt_rand(16,24); $i++) {
            $this->newAttack($home_team_diff,$away_team_diff,$match);
        }
        $match->played = 1;
    }

    // Simulates every Attack
    public function newAttack($home_team_diff,$away_team_diff,$match)
    {
        $rev_shoot_prob = mt_rand(0, 10);
        /* 20% Shoot chance for both home and away team
           60% chance shoot chance will not appear for both team */
        if($rev_shoot_prob < 2){
            // Chance for a shoot to be successful for home team
            if (mt_rand(0, 300) > (150 + $home_team_diff)) {
                // Goal score for home team
                $match->home_score += 1;
            }
        }elseif($rev_shoot_prob < 4){
            // Chance of a shoot to be successful for away team
            if (mt_rand(0, 300) > (150 + $away_team_diff)) {
                // Goal score for away team
                $match->away_score += 1;
            }
        }
    }

    public function resetLeagueMatches($league_id)
    {
        LeagueController::createLeagueFixture($league_id);
        LeagueController::setLeagueWeek($league_id, 1);
/*        Match::whereHas('home_team', function ($query) use ($league_id) {
            $query->where('league_id', $league_id);
        })->delete();*/
    }

    public function playAllWeekMatches($league_id)
    {
        $allMatches = Match::whereHas('home_team', function ($query) use ($league_id) {
            $query->where('league_id', $league_id);
        })->where('played', 0)->with('home_team')->with('away_team')->get();
        foreach ($allMatches as $match) {
            $this->playMatch($match);
            $match->save();
        }
        LeagueController::setLeagueWeek($league_id, $allMatches->max('week'));
    }

    public function simulateRemainingMatchesAndPredictWinner($league_id, $league)
    {
        $leagueMatches = Match::whereHas('home_team', function ($query) use ($league_id) {
            $query->where('league_id', $league_id);
        })->get();
        $allTeamIds = $this->getAllTeamIds($leagueMatches);
        $resultRatioSet = $this->createResultingRatioSet($allTeamIds);
        $playableGames =  $leagueMatches->where('played',0)->count();
        $stat = ceil(30 + (10000 / ($playableGames + 1)));
        for ($i = 0; $i < $stat; $i++) {
            $simulationResult = $this->simulateOnce($leagueMatches,$allTeamIds);
            foreach ($simulationResult as $key){
                $resultRatioSet[$key] += (1 / sizeof($simulationResult));
            }
        }
        foreach ($resultRatioSet as $key => $publishIntoCollection) {
            $league->teams->where('id', $key)->first()->win_ratio = ($publishIntoCollection / ($stat / 100));
        }
    }

    public function getTotalWeek($league_id)
    {
        return Match::whereHas('home_team', function ($query) use ($league_id) {
            $query->where('league_id', $league_id);
        })->max('week');
    }

    public function simulateOnce($leagueMatches,$teamIds){
        $teamPoints = array_fill_keys($teamIds, 0);
        $tempMatches = clone $leagueMatches;
        foreach ($tempMatches as $tempMatch) {
            $singleMatch = $tempMatch->replicate();
            if ($singleMatch->played == 0) {
                $this->playMatch($singleMatch);
            }
            if ($singleMatch->home_score == $singleMatch->away_score) {
                $teamPoints[$singleMatch->home_team_id] += 1;
                $teamPoints[$singleMatch->away_team_id] += 1;
            } elseif ($singleMatch->home_score > $singleMatch->away_score) {
                $teamPoints[$singleMatch->home_team_id] += 3;
            } else {
                $teamPoints[$singleMatch->away_team_id] += 3;
            }
        }
        $maxVal = max($teamPoints);
        $maxKey = array_keys($teamPoints, $maxVal);
        return $maxKey;
    }

    public function createResultingRatioSet($allTeamIds){
        return array_fill_keys($allTeamIds, 0);
    }
    public function getAllTeamIds($leagueMatches){
        return $leagueMatches->unique('home_team')->pluck('home_team.id')->toArray();
    }
}
