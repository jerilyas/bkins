<?php

namespace App\Http\Controllers;

use App\Helpers\MathHelper;
use App\Models\League;
use App\Models\Match;
use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LeagueController extends Controller
{
    public function newLeague()
    {
        $leagues = League::all();
        return view('league-select')->with('leagues', $leagues);
    }

    public function leagueFixture($league_id)
    {
        $this->checkFixture($league_id);
        $currentWeek = $this->getLeagueWeek($league_id);
        $playMatches = new MatchController();
        $playMatches->playWeekMatches($league_id, $currentWeek);
        $league = MatchController::getLeaguePlayedMatches($league_id);
        $currentMatches = MatchController::getWeekMatches($league_id, $currentWeek);
        $activeWeekFormatted = MathHelper::formatOrderNumber($currentWeek);
        $this->calculateTeamPoints($league);
        $playMatches->simulateRemainingMatchesAndPredictWinner($league_id, $league);
        return view('league-fixture')->with('league', $league)->with('matches', $currentMatches)->with('current_week', $currentWeek)
            ->with('active_week', $activeWeekFormatted);
    }


    public static function createLeagueFixture($league_id)
    {
        Match::whereHas('home_team', function ($query) use ($league_id) {
            $query->whereHas('league', function ($q2) use ($league_id) {
                $q2->where('id', $league_id);
            });
        })->delete();
        $teams = Team::where('league_id', $league_id)->get()->shuffle();
        $combination = MathHelper::uniqueCombination($teams->count());
        foreach ($teams as $key => $home_team) {
            foreach ($teams as $key_2 => $away_team) {
                $week = $combination[$key + 1][$key_2 + 1];
                if ($week > 0) {
                    $newMatch = new Match();
                    $newMatch->home_team_id = $home_team->id;
                    $newMatch->away_team_id = $away_team->id;
                    $newMatch->week = $week;
                    $newMatch->save();
                }
            }
        }
    }

    public function checkFixture($league_id)
    {
        if (!$this->leagueHasFixture($league_id)) {
            $this->setLeagueWeek($league_id);
            $this->createLeagueFixture($league_id);
        }
    }

    public function calculateTeamPoints($league)
    {
        foreach ($league->teams as $team) {
            LeagueController::teamPoints($team);
        }
        $league->teams = $league->teams->sortByDesc('pts');
    }

    static function teamPoints($team)
    {
        $team->played = $team->home_matches->where('played', 1)->count() + $team->away_matches->where('played', 1)->count();
        $team->win = $team->home_matches->filter(function ($item) {
                return $item->home_score > $item->away_score;
            })->count() + $team->away_matches->filter(function ($item) {
                return $item->away_score > $item->home_score;
            })->count();
        $team->d = $team->home_matches->filter(function ($item) {
                return $item->home_score == $item->away_score;
            })->count() + $team->away_matches->filter(function ($item) {
                return $item->away_score == $item->home_score;
            })->count();
        $team->lose = $team->home_matches->filter(function ($item) {
                return $item->home_score < $item->away_score;
            })->count() + $team->away_matches->filter(function ($item) {
                return $item->away_score < $item->home_score;
            })->count();
        $team->goal_difference = $team->home_matches->where('played', 1)->sum('home_score') + $team->away_matches->where('played', 1)->sum('away_score')
            - $team->home_matches->where('played', 1)->sum('away_score') - $team->away_matches->where('played', 1)->sum('home_score');
        $team->pts = ($team->win * 3) + ($team->d);
        return $team;
    }

    static function setLeagueWeek($league_id, $week = 1)
    {
        Session::put('league_week_' . $league_id, $week);
    }

    static function getLeagueWeek($league_id)
    {
        if (!Session::has('league_week_' . $league_id)) {
            LeagueController::setLeagueWeek($league_id);
        }
        return Session::get('league_week_' . $league_id);
    }

    static function leagueHasFixture($league_id): bool
    {
        return League::where('id', $league_id)->whereHas('teams.home_matches')->count() > 0;
    }
}
