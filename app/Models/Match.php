<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use HasFactory;

    public function home_team(){
        return $this->belongsTo(Team::class,'home_team_id');
    }

    public function away_team(){
        return $this->belongsTo(Team::class,'away_team_id');
    }
}
