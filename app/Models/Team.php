<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;
    public function league(){
        return $this->belongsTo(League::class,'league_id');
    }
    public function home_matches(){
        return $this->hasMany(Match::class,'home_team_id');
    }
    public function away_matches(){
        return $this->hasMany(Match::class,'away_team_id');
    }
}
