<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MathHelper
{
    static function uniqueCombination($size): array
    {
        $games = [];
        for ($i = 1; $i <= $size; $i++) {
            $games[$i] = array();
            for ($j = 1; $j <= $size; $j++) {
                $games[$i][$j] = MathHelper::calculateWeek($i, $j, $size);
            }
        }

        return $games;
    }

    static function calculateWeek($variation_1, $variation_2, $total_size): int
    {
        if ($variation_1 == $variation_2) {
            return -1;
        }
        $week = $variation_1 + $variation_2 - 2;
        if ($week >= $total_size) {
            $week -= ($total_size - 1);
        }
        if ($variation_1 > $variation_2) {
            $week += $total_size - 1;
        }
        return $week;
    }

    static function formatOrderNumber($number): string
    {
        $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
        return $numberFormatter->format($number);
    }
}
